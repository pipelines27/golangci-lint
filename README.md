# golangci-lint Pipeline

[![pipeline status](https://gitlab.com/pipelines27/golangci-lint/badges/main/pipeline.svg)](https://gitlab.com/pipelines27/golangci-lint/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

Pipeline to quickly lint Go projects.

- Enforces Linting with [golangci-lint](https://golangci-lint.run/)
